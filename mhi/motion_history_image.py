# -*- coding: utf-8 -*-

import cv2
import numpy as np


class MotionHistoryImage:

    def __init__(self, tau, initial_frame):
        self.tau = tau
        self.mhi = np.zeros(initial_frame.shape[:2], dtype=np.float64)
        self.previous_frame = None

    def add_frame(self, frame):
        gray_image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # blurred_image = cv2.medianBlur(gray_image, 5)
        blurred_image = cv2.GaussianBlur(gray_image, (5, 5), 0)

        if self.previous_frame is None:
            self.previous_frame = blurred_image
            return np.zeros_like(blurred_image)

        theta = 6.0
        binary_image = np.zeros_like(blurred_image)
        frame_difference = np.absolute(blurred_image.astype(np.float64) \
                                    - self.previous_frame.astype(np.float64))
        binary_image[np.where(frame_difference >= theta)] = 1

        self.previous_frame = blurred_image

        # Morphological Open - Erode then Dilate
        kernel = np.ones((4, 4), np.uint8)
        binary_image = cv2.morphologyEx(binary_image, cv2.MORPH_OPEN, kernel)

        # If binary image == 1, set the value to Tau
        self.mhi[np.where(binary_image == 1.0)] = self.tau

        # If binary image == 0, reduce the value by 1
        self.mhi[np.where(binary_image == 0.0)] -= 1.0
        # If MHI < 0, then set the value to 0
        negative_indices = np.where(self.mhi < 0.0)
        self.mhi[negative_indices] = 0.0

        return binary_image


def generate_central_moments(motion_image):
        rows, cols = motion_image.shape[:2]
        x, y = np.meshgrid(np.arange(cols), np.arange(rows))

        m00 = np.sum(motion_image)
        m10 = np.sum(x * motion_image)
        m01 = np.sum(y * motion_image)
        x_avg = m10 / m00
        y_avg = m01 / m00

        mu20 = np.sum(((x - x_avg) ** 2) * ((y - y_avg) ** 0) * motion_image)
        mu11 = np.sum(((x - x_avg) ** 1) * ((y - y_avg) ** 1) * motion_image)
        mu02 = np.sum(((x - x_avg) ** 0) * ((y - y_avg) ** 2) * motion_image)
        mu30 = np.sum(((x - x_avg) ** 3) * ((y - y_avg) ** 0) * motion_image)
        mu21 = np.sum(((x - x_avg) ** 2) * ((y - y_avg) ** 1) * motion_image)
        mu12 = np.sum(((x - x_avg) ** 1) * ((y - y_avg) ** 2) * motion_image)
        mu03 = np.sum(((x - x_avg) ** 0) * ((y - y_avg) ** 3) * motion_image)
        mu22 = np.sum(((x - x_avg) ** 2) * ((y - y_avg) ** 2) * motion_image)

        return [mu20, mu11, mu02, mu30, mu21, mu12, mu03, mu22]


def generate_scale_invariant_moments(c_moments, motion_image):
        mu00 = np.sum(motion_image)
        mu20, mu11, mu02, mu30, mu21, mu12, mu03, mu22 = c_moments

        v20 = mu20 / (mu00 ** (1.0 + 2.0 / 2.0))
        v11 = mu11 / (mu00 ** (1.0 + 2.0 / 2.0))
        v02 = mu02 / (mu00 ** (1.0 + 2.0 / 2.0))
        v30 = mu30 / (mu00 ** (1.0 + 3.0 / 2.0))
        v21 = mu21 / (mu00 ** (1.0 + 3.0 / 2.0))
        v12 = mu12 / (mu00 ** (1.0 + 3.0 / 2.0))
        v03 = mu03 / (mu00 ** (1.0 + 3.0 / 2.0))
        v22 = mu22 / (mu00 ** (1.0 + 4.0 / 2.0))

        return [v20, v11, v02, v30, v21, v12, v03, v22]
