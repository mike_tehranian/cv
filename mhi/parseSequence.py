# -*- coding: utf-8 -*-

import csv
import numpy as np

SEQUENCE_FILE = '00sequences.txt'
MY_SEQUENCE_FILE = 'my_videos_sequences.txt'

def get_files_and_frames():

    data = {}
    lol = list(csv.reader(open(SEQUENCE_FILE, 'rb'), delimiter='\t'))

    for i in range(21,644):
        if len(lol[i]) < 3:
            continue
        if len(lol[i]) == 3:
            frame_index = 2
        else:
            frame_index = 3
        frames = lol[i][frame_index].split(',')
        cleaned = [tuple(map(int, (x.strip()).split('-'))) for x in frames]
        video_id = str(lol[i][0]).strip() + str('_uncomp.avi')
        data[video_id] = cleaned

    # print data

    return data

def find_average_frame_length():
    data = get_files_and_frames()
    sum_diff, count = 0.0, 0.0

    for video_name, video_frames in data.iteritems():
        for pair in video_frames:
            diff = pair[1] - pair[0]
            sum_diff += diff
            count += 1

    print "Avererage lenght {}".format(float(sum_diff) / count)

def get_my_files_and_frames():
    data = {}
    lol = list(csv.reader(open(MY_SEQUENCE_FILE, 'rb'), delimiter='\t'))

    for i in range(4):
        if len(lol[i]) < 2:
            continue
        actions = lol[i][1].split(',')
        cleaned = tuple([x.strip() for x in actions])
        video_id = str(lol[i][0]).strip()
        data[video_id] = cleaned

    # print data

    return data


if __name__ == '__main__':
    find_average_frame_length()
