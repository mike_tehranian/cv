Report: final_project_report.pdf

My code is based on the popular SciPy software stack.
The libraries needed to run my code are shown below:
* Sci-kit learn
* Matplotlib
* OpenCV
* Numpy

How To Run My Code:
$ python main.py

Source Files:
* main.py - main driver script
* motion_history_image.py - calculates MHI and Hu moments
* parseSequence.py - parses training video meta-data txt files

If you would like to train the classifier:
* Please download and unzip all videos from here: http://www.nada.kth.se/cvap/actions
and copy them to the folder: dataset/all_videos
* Uncomment out lines 398-404, 407-412, 415-419, 427-428 in main.py

If you would like to create the annotated videos:
* Uncomment out line 440 in main.py
