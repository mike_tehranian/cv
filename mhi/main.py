# -*- coding: utf-8 -*-
import os

import cv2
import itertools
import numpy as np
import pickle

from sklearn.neighbors import KNeighborsClassifier
import sklearn.model_selection as ms

from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.svm import SVC, LinearSVC

from sklearn.learning_curve import learning_curve
from sklearn.learning_curve import validation_curve

from sklearn.metrics import accuracy_score, confusion_matrix, make_scorer

import matplotlib.pyplot as plt

import parseSequence
import motion_history_image


action_y_values = {
        'boxing': 0,
        'handclapping': 1,
        'handwaving': 2,
        'jogging': 3,
        'running': 4,
        'walking': 5
}

y_action_values = ['boxing', 'handclapping', 'handwaving',
                   'jogging', 'running', 'walking']


def generate_mhi_for_video(video_filename, video_frames):
    print "Video file: {} Frames: {}".format(video_filename, video_frames)

    video = cv2.VideoCapture(video_filename)

    num_video_frames = len(video_frames)
    mhi_frames = [None] * num_video_frames
    mhi_images = [None] * num_video_frames
    frame_images = [None] * num_video_frames
    frame_id = 0

    while True:
        ret, frame = video.read()
        if not ret:
            break

        frame_id += 1

        for v_idx, current_frame_range in enumerate(video_frames):
            if frame_id == current_frame_range[0]:
                # print "Start Frame: {} {}".format(frame_id, current_frame_range)
                # Should I divide Tau by a constant below?
                current_tau = current_frame_range[1] - current_frame_range[0]
                mhi = motion_history_image.MotionHistoryImage(current_tau, frame)
                motion_image = mhi.add_frame(frame)
                mhi_frames[v_idx] = mhi
                frame_images[v_idx] = [motion_image]
            elif frame_id == current_frame_range[1]:
                # print "End Frame: {} {}".format(frame_id, current_frame_range)
                mhi = mhi_frames[v_idx]
                mhi_image = mhi.add_frame(frame)
                frame_images[v_idx].append(mhi_image)
                mhi_images[v_idx] = mhi.mhi / np.max(mhi.mhi)
            elif current_frame_range[0] < frame_id < current_frame_range[1]:
                # print "Process Frame: {} {}".format(frame_id, current_frame_range)
                mhi = mhi_frames[v_idx]
                mhi_image = mhi.add_frame(frame)
                frame_images[v_idx].append(mhi_image)


    video.release()

    return mhi_images


def extract_frames_for_video(video_filename):
    video = cv2.VideoCapture(video_filename)

    mhi = None
    frame_images = None
    frame_id = 0
    binary_image = None
    last_binary_image = None
    recording_mhi = False
    mhi_frames = []
    tau = int(video.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))
    start_frame = None
    end_frame = None

    while True:
        ret, frame = video.read()
        if not ret:
            break

        frame_id += 1
        last_binary_image = binary_image

        if frame_id == 1:
            mhi = motion_history_image.MotionHistoryImage(tau, frame)
            binary_image = mhi.add_frame(frame)
            frame_images = [binary_image]
        elif 1 < frame_id < tau:
            binary_image = mhi.add_frame(frame)
            frame_images.append(binary_image)
        elif frame_id == tau:
            binary_image = mhi.add_frame(frame)
            frame_images.append(binary_image)

        if frame_id != 1:
            difference = ((binary_image - last_binary_image) ** 2).sum()
            # print "Diff {}".format(difference)
            if difference < 10 or frame_id == tau:
                if recording_mhi is True:
                    # print "End Recording {}".format(frame_id)
                    if frame_id - start_frame >= 32:
                        mhi_frames.append((start_frame, frame_id))
                        # MDT Added constant Tau below
                        mhi_frames.append((start_frame, start_frame + 30))
                recording_mhi = False
            else:
                if recording_mhi is not True:
                    # print "Start Recording {}".format(frame_id)
                    start_frame = frame_id
                recording_mhi = True

    # If the entire video contains an action
    if len(mhi_frames) == 0 and start_frame is not None \
            and start_frame != frame_id and (frame_id - start_frame) >= 32:
        mhi_frames.append((start_frame, frame_id))
        mhi_frames.append((start_frame, start_frame + 30))

    video.release()

    return mhi_frames


def generate_moments_for_people(people_set, video_frames_dict):
    video_metadata = []
    moments_per_frame = []
    activity_label = []

    for video_name in sorted(video_frames_dict):
        video_frames = video_frames_dict[video_name]
        video_filename = os.path.join("dataset", "all_videos", video_name)

        if people_set is not None:
            file_fields = video_name.strip().split("_")
            person, action, condition = file_fields[:3]
            label = action_y_values[action]

            if person not in people_set:
                continue
            # if action != 'boxing':
            #     continue
            # if person != 'person05':
            #     continue
            # if condition != 'd2':
            #     continue

            print "Generating Moments: {}, {}, {}".format(person, action, condition)
        else:
            actions = video_name.strip()[:-4].split("_")
            labels = [x for x in actions for _ in (0, 1)]
            data_index = 0

        mhi_images = generate_mhi_for_video(video_filename, video_frames)

        for mhi_idx, mhi in enumerate(mhi_images):
            # normalized_mhi = np.uint8(mhi * 255)
            # cv2.imshow('mhi_images', normalized_mhi)
            # cv2.waitKey(0)

            mei = np.uint8(mhi > 0)
            cm_mhi, cm_mei = motion_history_image.generate_central_moments(mhi), \
                             motion_history_image.generate_central_moments(mei)
            sim_mhi, sim_mei = motion_history_image.generate_scale_invariant_moments(cm_mhi, mhi), \
                               motion_history_image.generate_scale_invariant_moments(cm_mei, mei)
            moments_per_frame.append(cm_mhi + cm_mei + sim_mhi + sim_mei)

            if people_set is not None:
                activity_label.append(label)
            else:
                activity_label.append(action_y_values[labels[data_index]])
                data_index += 1

    return np.array(moments_per_frame), np.array(activity_label)


def extract_all_videos_frames(people_set, video_frames_dict):
    generated_video_frames_dict = {}

    for video_name in sorted(video_frames_dict):
        video_frames = video_frames_dict[video_name]
        video_filename = os.path.join("dataset", "all_videos", video_name)

        person, action, condition = video_name.strip().split("_")[:3]

        if people_set is not None and person not in people_set:
            continue
        # if action != 'boxing':
        #     continue
        # if person != 'person05':
        #     continue
        # if condition != 'd2':
        #     continue

        mhi_frames = extract_frames_for_video(video_filename)

        if len(mhi_frames) == 0:
            continue

        generated_video_frames_dict[video_name] = tuple(mhi_frames)

    return generated_video_frames_dict

def knn_predict(moments_tv, labels_tv, moments_me, labels_me):
    X_train = np.vstack((moments_tv, moments_me))
    y_train = np.hstack((labels_tv, labels_me))

#     classifier = KNeighborsClassifier(metric='euclidean', n_neighbors=1)
#     classifier.fit(X_train, y_train)
#     labels_pred = classifier.predict(X_train)
#     score = accuracy_score(labels, labels_pred)
#     print "Score: {}".format(score)

#     return classifier

    classifier = KNeighborsClassifier()
    params = {
              'metric': ['manhattan', 'euclidean', 'chebyshev'],
              # 'n_neighbors': np.arange(1,21,1),
              'n_neighbors': np.arange(1,9,1),
              'weights': ['uniform', 'distance']
             }
    opt = ms.GridSearchCV(
            estimator=classifier,
            param_grid=params,
            cv=10
    )
    gs = opt.fit(X_train, y_train)
    print "Best score: {}".format(gs.best_score_)
    print "Best params: {}".format(gs.best_params_)

    pickle.dump(gs.best_estimator_, open("knn_my_test.p", "wb"))

    return gs.best_estimator_

def boost_predict(moments_tv, labels_tv, moments_me, labels_me):
    X_train = np.vstack((moments_tv, moments_me))
    y_train = np.hstack((labels_tv, labels_me))
    classifier = DecisionTreeClassifier(criterion='entropy', random_state=0)

    depths = range(2,18)
    params = {
              'max_depth': depths
             }

    opt = ms.GridSearchCV(
            estimator=classifier,
            param_grid=params,
            cv=10)

    gs = opt.fit(X_train, y_train)
    print "WL Best score: {}".format(gs.best_score_)
    print "WL Best params: {}".format(gs.best_params_)

    opt = gs.best_estimator_

    boost = AdaBoostClassifier(base_estimator=opt)
    # estimator_range = range(2,60)
    estimator_range = range(40,60)
    learning_range = [(1e-1)*10**-0.5,1e-1,(1e-2)*10**-0.5,
            1e-2,(1e-3)*10**-0.5,1e-3,1,2,3,4,5,6,7,8,9,10]
    #learning_range = [1]
    params = {
              'n_estimators': estimator_range,
              'learning_rate': learning_range
             }

    opt = ms.GridSearchCV(
            estimator=boost,
            param_grid=params,
            cv=10)

    gs = opt.fit(X_train, y_train)
    print "Best score: {}".format(gs.best_score_)
    print "Best params: {}".format(gs.best_params_)

    pickle.dump(gs.best_estimator_, open("boosting_my_test.p", "wb"))

    return gs.best_estimator_

def svm_predict(moments_tv, labels_tv, moments_me, labels_me):
    X_train = np.vstack((moments_tv, moments_me))
    y_train = np.hstack((labels_tv, labels_me))
    pipe_lr = SVC()

    num_train = X_train.shape[0]
    #gamma_range_one = np.arange(0.2,2.1,0.2)
    # gamma_range1 = np.logspace(-6, -1, 5)
    gamma_range2 = np.logspace(-6, 2.8, 8)
    # gamma_range3 = np.logspace(-6, 1.01, 8)
    params = {
              # 'kernel': ['linear', 'poly', 'rbf', 'sigmoid'],
              'kernel': ['rbf', 'sigmoid'],
              'C': gamma_range2,
              'gamma': gamma_range2
              #'n_iter': [int((1e6/num_train)/.8) + 1]
             }

    opt = ms.GridSearchCV(
            estimator=pipe_lr,
            param_grid=params,
            cv=10)

    gs = opt.fit(X_train, y_train)
    print "SVM Best score: {}".format(gs.best_score_)
    print "SVM Best params: {}".format(gs.best_params_)

    pickle.dump(gs.best_estimator_, open("svm_my_test.p", "wb"))

    return gs.best_estimator_

def annotate_my_video_with_actions(best_estimator):
    moments, labels, generated_my_video_frames_dict = \
                            load_my_test_data()
    X_values = moments
    labels_pred = best_estimator.predict(X_values)
    y_idx_frame = 0

    for video_name in sorted(generated_my_video_frames_dict):
        frame_id = 0
        video_frames = generated_my_video_frames_dict[video_name]
        video_filename = os.path.join("dataset", "all_videos", video_name)
        video = cv2.VideoCapture(video_filename)

        while True:
            ret, frame = video.read()
            if not ret:
                break

            frame_id += 1
            if frame_id == 1:
                out = cv2.VideoWriter(os.path.join('output', 'annotated_' + video_name),
                                      cv2.cv.CV_FOURCC(*'MP4V'),
                                      30,
                                      (frame.shape[1], frame.shape[0]))

            for idx, current_frame_range in enumerate(video_frames):
                if current_frame_range[0] <= frame_id <= current_frame_range[1]:
                    cv2.putText(frame, y_action_values[labels_pred[y_idx_frame+idx]],
                            (175,300), cv2.FONT_HERSHEY_SIMPLEX, 2, (0,0,255), 3)

            out.write(frame)
            cv2.imshow('frame', frame)
            cv2.waitKey(1)

        out.release()
        video.release()
        y_idx_frame += len(video_frames)

def activity_recognition():
    video_frames_dict = parseSequence.get_files_and_frames()
    training_people = set(['person11', 'person12', 'person13', 'person14', 'person15',
                          'person16', 'person17', 'person18'])
    validation_people = set(['person19', 'person20', 'person21', 'person23',
                            'person24', 'person25', 'person01', 'person04'])
    test_people = set(['person22', 'person02', 'person03', 'person05', 'person06',
                      'person07', 'person08', 'person09', 'person10'])
    all_people = training_people.union(validation_people).union(test_people)
    my_test_videos_dict = parseSequence.get_my_files_and_frames()

    #### DATA
    # All People - Using the Frames
    # cm_all, sim_all, labels_all = generate_moments_for_people(
    #                                                 all_people,
    #                                                 video_frames_dict)

    # Train and Validation - Using the Frames
    # cm_tv, sim_tv, labels_tv = generate_moments_for_people(
    #                                                 training_people.union(validation_people),
    #                                                 video_frames_dict)
    # Test - Using the Frames
    # cm_test, sim_test, labels_test = generate_moments_for_people(
    #                                                 test_people,
    #                                                 video_frames_dict)

    # Train and Validation - Not Using the Frames
    # generated_video_frames_dict = extract_all_videos_frames(
    #                       training_people.union(validation_people),
    #                       video_frames_dict)
    # moments_per_frame_tv, labels_tv = generate_moments_for_people(
    #                                                 training_people.union(validation_people),
    #                                                 generated_video_frames_dict)
    # save_tv_data(moments_per_frame_tv, labels_tv)

    # Test - Not Using the Frames
    # generated_video_frames_dict = extract_all_videos_frames(
    #                                                test_people, video_frames_dict)
    # moments_per_frame_test, labels_test = generate_moments_for_people(
    #                                  test_people,
    #                                  generated_video_frames_dict)
    # save_test_data(moments_per_frame_test, labels_test)

    # Test - Not Using the Frames - My Videos
    # generated_my_video_frames_dict = extract_all_videos_frames(None, my_test_videos_dict)
    # my_moments_per_frame_test, my_labels_test = generate_moments_for_people(None,
    #                                                     generated_my_video_frames_dict)
    # save_my_test_data(my_moments_per_frame_test, my_labels_test,
    #                                generated_my_video_frames_dict)

    # Load Features
    # moments_per_frame_tv, labels_tv = load_tv_data()
    # my_moments_per_frame_test, my_labels_test, _ = load_my_test_data()

    #### CLASSIFIERS
    # Classifier - Not using the Frames
    # my_best_estimator = boost_predict(moments_per_frame_tv, labels_tv,
    #                                   my_moments_per_frame_test, my_labels_test)
    # my_best_estimator = knn_predict(moments_per_frame_tv, labels_tv,
    #                                 my_moments_per_frame_test, my_labels_test)
    # my_best_estimator = svm_predict(moments_per_frame_tv, labels_tv,
    #                                 my_moments_per_frame_test, my_labels_test)

    # Load Classifier
    my_best_estimator = load_classifier()

    # Testing when NOT knowing the frames
    test_with_classifier(my_best_estimator)

    # annotate_my_video_with_actions(my_best_estimator)

    # Classification Graphs
    # learning_curve_plot(my_best_estimator, moments_per_frame_tv, labels_tv)
    # validation_curve_plot(my_best_estimator, moments_per_frame_tv, labels_tv)

def save_tv_data(moments_per_frame_tv, labels):
    moments_per_frame_tv.dump("moments_tv_no_frames.p")
    labels.dump("labels_tv_no_frames.p")

def load_tv_data():
    return np.load("moments_tv_no_frames.p"), \
           np.load("labels_tv_no_frames.p")

def save_test_data(moments_per_frame_test, labels_test):
    moments_per_frame_test.dump("moments_test_no_frames.p")
    labels_test.dump("labels_test_no_frames.p")

def load_test_data():
     return np.load("moments_test_no_frames.p"), \
            np.load("labels_test_no_frames.p")

def save_my_test_data(moments_per_frame_my_test, my_labels_test,
                                 generated_my_video_frames_dict):
    moments_per_frame_my_test.dump("moments_my_test.p")
    my_labels_test.dump("labels_my_test.p")
    pickle.dump(generated_my_video_frames_dict, open("my_videos_test_frames.p", "wb"))

def load_my_test_data():
    return np.load("moments_my_test.p"), \
           np.load("labels_my_test.p"), \
           np.load("my_videos_test_frames.p")

def load_classifier():
    return np.load("boosting_my_test.p")

def test_with_classifier(best_estimator):
    moments_test, labels = load_test_data()
    X_values = moments_test

    labels_pred = best_estimator.predict(X_values)

    score = accuracy_score(labels, labels_pred)
    norm_score = accuracy_score(labels, labels_pred, normalize=False)

    print "Test Score: {}, Non-Norm Test Score: {}".format(score, norm_score)
    show_classifier_confusion(labels, labels_pred)

def show_classifier_confusion(labels, labels_pred):
    class_names = list(sorted(action_y_values, key=action_y_values.get))
    conf_matrix = confusion_matrix(labels, labels_pred)
    print "Confusion Matrix:\n {}".format(conf_matrix)

    # Show confusion matrix in a separate window
    plt.matshow(conf_matrix)
    plt.title('Confusion matrix')
    plt.colorbar()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

    plt.figure()
    plot_confusion_matrix(conf_matrix,
                          class_names,
                          title='Confusion matrix, without normalization')
    plt.figure()
    plot_confusion_matrix(conf_matrix,
                          class_names,
                          normalize=True,
                          title='Normalized confusion matrix')
    plt.show()

# Code taken from:
# http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html
def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')


def learning_curve_plot(best_estimator, X_train, y_train):
    train_sizes, train_scores, test_scores = \
            learning_curve(estimator=best_estimator,
                           X=X_train,
                           y=y_train,
                           train_sizes=np.linspace(0.1, 1.0, 10),
                           cv=10,
                           n_jobs=1)

    train_mean = np.mean(train_scores, axis=1)
    train_std = np.std(train_scores, axis=1)
    test_mean = np.mean(test_scores, axis=1)
    test_std = np.std(test_scores, axis=1)

    # Training line
    plt.plot(train_sizes, train_mean,
              color='blue', marker='o',
              markersize=5,
              label='Training accuracy')
    plt.fill_between(train_sizes,
                      train_mean + train_std,
                      train_mean - train_std,
                      alpha=0.15, color='blue')

    # Test line
    plt.plot(train_sizes, test_mean,
              color='green', linestyle='--',
              marker='s', markersize=5,
              label='Validation Accuracy')
    plt.fill_between(train_sizes,
                      test_mean + test_std,
                      test_mean - test_std,
                      alpha=0.15, color='green')
    plt.grid()
    plt.xlabel('Number of training samples')
    plt.ylabel('Accuracy')
    plt.legend(loc='best')
    plt.title('AdaBoost Learning Curve', loc='center')
    plt.show()


def validation_curve_plot(best_estimator, X_train, y_train):
    param_range = range(1,60)
    train_scores, test_scores = validation_curve(
                 estimator=best_estimator,
                 X=X_train,
                 y=y_train,
                 param_name='n_estimators',
                 param_range=param_range,
                 cv=10)

    train_mean = np.mean(train_scores, axis=1)
    train_std = np.std(train_scores, axis=1)
    test_mean = np.mean(test_scores, axis=1)
    test_std = np.std(test_scores, axis=1)

    plt.plot(param_range, train_mean,
          color='blue', marker='o',
          markersize=5,
          label='training accuracy')
    plt.fill_between(param_range, train_mean + train_std,
                  train_mean - train_std, alpha=0.15,
                  color='blue')
    plt.plot(param_range, test_mean,
          color='green', linestyle='--',
          marker='s', markersize=5,
          label='validation accuracy')
    plt.fill_between(param_range,
                  test_mean + test_std,
                  test_mean - test_std,
                  alpha=0.15, color='green')
    plt.grid()
    plt.xscale('log')
    plt.legend(loc='best')
    plt.xlabel('Number of Estimators')
    plt.ylabel('Accuracy')
    plt.title('AdaBoost Validation Curve', loc='center')
    plt.show()


if __name__ == "__main__":
    activity_recognition()
